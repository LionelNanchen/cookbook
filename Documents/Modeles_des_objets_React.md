# Modèles des objets React

###Une recette

```javascript
const recipe = {
  href: String,
  chief: User,
  image: Image,
  name: String,
  description: String,
  preparationTime: Number,
  yield: Number,
  rating: Number,
  totalRating: Number,
  ingredientsCategories: [{
    category: String,
    ingredients: [{
      quantity: String,
      name: String,
    },]
  }],
  steps: [{
    iteration: Number,
    explanation: String,
  }],
  tags: [String]
};
```

### Un Utilisateur

```javascript
const user = {
    email: String,
    name: String,
    picture: Image,
    recipes: [{}] //un tableau de recipe
    followers: [{}],//un tableau de user
  	followed: [{}],//un tableau de user
  	notifications: [{ type: ObjectId, ref: 'Notification' }],
};
```

