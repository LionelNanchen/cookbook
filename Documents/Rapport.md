# Cookbook - Projet MAC/ TWEB

###Auteurs

- Allemand Adrien
- Krug Loyse
- Nanchen Lionel
- Nicole soit Nicoulaz Olivier

### Enseignants

- Fatemi Nastaran

- Ntawuruhunga Paul
- Santamaria Miguel

### Lien de Cookbook

https://cookbook-heig-client.herokuapp.com/

## Technologies:

###Front-end:

- React
- Material-UI

Nous avons utilisé ces 2 technologies, car pour React nous l'avions vu en cours TWEB et pour Material-UI car c'est une des librairies React les plus connues et utilisée.

###Back-end:

- Node.js
- Express.js
- GraphQL -GraphQL Express
- MongoDB/ Mongoose
- Mongo Atlas

Concernant Node.js et Express.js ce sont deux technologies essentielles pour la création d'un back-end. Nous avons choisi MongoDB comme base de données car nous la connaissons bien, nous l'avions déjà utilisée lors de précédents projets. Nous avons hébergé la base de données sur Mongo Atlas afin de bénéficier d'un hébergement de qualité gratuitement.

## API

Après discussion avec l'assistant, nous avons décidé de choisir GraphQL, comme nous le conseillait l'assistant, car cette technologie faisant partie du cours TWEB, il était intéressant de l'utiliser dans ce projet.

Pour cette partie du projet, nous avons décidé de partir sur une architecture MVC afin de bien séparer notre code. Nous avons trouvé beaucoup d'exemples sur internet qui nous ont aidé à bien structurer notre projet. Vous trouverez donc les dossiers models, controllers et routes qui sont utilisés par `app.js` afin de résoudre les endpoints.

### GraphQL

Pour la communication entre Client Serveur et Base de Données, nous avons choisi d'utiliser l'API GraphQL de manière à pouvoir créer des requêtes structurées et obtenir en une seule requête depuis le client toutes les informations nécessaires au peuplement d'une page. 

Au niveau GraphQL, nous avons écrit une API contenant tous les objets récupérables et toutes les fonctions pour les trouver/créer/modifier. Ce fichier se trouve dans le dossier `app/` de l'application, côté API.

Les resolvers  se trouvent dans le dossier `app/graphql`de l'application. 

Le défi principale de graphQL dans ce projet aura été de réussir à mettre en place la connexion entre tous les éléments (modèles de donnée, resolvers, api, enpoints).

## Requêtes et données

### Base de donnée NoSQL

Pour le projet, nous avons choisi d'utiliser la base de donnée MongoDB. Les fichiers liés à la création de models se situe dans le dossier `model/` de l'application côté serveur. 

Nous avons créé deux modèles principaux: 

-  User
-  Recipe 

Ces deux modèles font appel eux-mêmes à des schémas intégrés en tant qu'objets embedded: 

- Notification 
- Feedback
- Step
- Ingredient 
- IngredientCategory

Ces schémas n'ont pas été utilisés pour créer des models, mais possièdent nécanmoins un identifiant créé par la DB. Cet identifiant a pu être mis à profis dans le projet  notemmant pour permettre à l'utilisateur de supprimer des notifications sur sa page. 

### Modèles de données

dans les modèles de données nous avons fait des choix particuliers liés à la gestion des notifications. 

### Follow

Concernant le fait de suivre un utilisateur, nous avons décidé de stocker deux tableaux distinct dans chaque utilisateur. Ces tableau sont "followed" et "followers" qui stockent respectivement les personnes que l'on suis et les personnes qui nous suivent. Ceci a pour but d'éviter de devoir parcourir les utilisateurs afin de savoir qui nous suis. 

Par contre ce choix implique que l'on doit mettre à jour des données cohérentes sur chaque utilisateur. Nous avons réalisé ce point dans la partie GraphQL du projet côté API.

### Notifications

Lorsque nous nous sommes penchés sur le problème des notifications, nous avons pensé à plusieurs scenarios. 

1. Le user va rechercher les notifications qui lui appartiennent dans un "tableau" de notifications
2. On stock un tableau de notifications dans le user afin d'y avoir accès directement

Nous avons choisi l'option 2 car c'est, selon nous, la méthode la plus économme en requêtes même si elle nous aura demandé plus de temps à implémenter.

## Recherches avancées et notifications

Nous n'avons pas réussi dans ce projet à implémenter toutes les fonctionnalités de recherche avancée. 

Néamoins nous avons implémentés quelques points allant dans ce sens. 

### Le classement des recettes

Nous avons mis en place un système de classement des recettes, se basant sur une note que chaque utilisateur peut attribuer à une recette (et modifier par la suite au gré de ses envies). 

Une recette possède ainsi une note moyenne sur 5 étoiles, basés sur les notes reçues. 

Ces éléments ont été utilisés pour permettre lors d'une recherche, de renvoyer des résultats par ordre de pertinence: tout d'abord en fonction de la note, puis en fonction du nombre de reviews (une recette étant notée à 5 avec 500 reviews ayant plus de poids qu'une recette notés à 5 avec 2 reviews)

### Recherche d'un user par nom

Il est possible de chercher un user en écrivant une partie de son nom dans la barre de recherche. Un regex se charge ensuite de chercher parmis tous les utilisateurs, si certain possèdent cette chaîne de caractères dans leur nom.

### Recherche par nom et par tags

De la même manière qu'on peut chercher un user, on peut faire la même recherche au  niveau des recettes. De plus à celles-ci on peut appliquer un filtre avec des tags pour récupérer les recettes qui contiennent une chaîne de caractère ET qui possèdent un certaine liste de tags. 

#### Recherche par tags non présents. 

Implémenté côté serveur, mais pas côté client, il est possible de faire des requêtes pour récupérer des recettes qui contiennent une certaine chaîne de caractère, ET qui possèdent une certaine liste de tags ET qui ne contiennent PAS une autre liste de tags. 

### Notifications

Notre serveur envoie des notifications aux users lors d'événements particuliers: 

- Création d'une nouvelle recette (envoyée à tous les followers de l'utilisateur)
- Abonnement à un utilisateur (cet utilisateur reçoit une notification pour le prévenir qu'il est suivit)
- Attribution d'une note à une recette (l'utilisateur est informé que sa recette a été notée)

## Déploiement

Pour le déploiement, nous avons choisi Heroku car nous l'avions utilisé pour le projet GitHub Analytic en cours TWEB.

Nous avons séparer notre application en deux parties, le front-end et le back-end. Ce deux parties sont déployées individuellement par Heroku.

## Utilisation

Premièrement pour utiliser notre application, l'utilisateur doit avoir un compte.

### Page Sign in

Pour que l'utilisateur puisse se connecter il doit entrer son adresse email et son mot de passe. Un bouton "Sign up" permet de créer un compte. S'il y a une erreur, une message d'erreur apparait.

### Page Sign up

Pour qu'un utilisateur puisse créer un compte, il doit fournir une adresse email, un pseudo et un mot de passe (ce mot de passe devra être répété dans la champ "Repeat password"). S'il y a une erreur, une message d'erreur apparait.

### Page User

Sur cette page, toutes les informations d'un utilisateur sont présentes.

Si l'utilisateur affiché est celui de l'utilisateur courant du site, une roue "option" est displonible. Dans le menu opiton, il peut modifier son avatar, son mot de passe et son adresse email.

Un "coeur" indique si l'utilisateur courant "follow" l'utilisateur affiché.

Les recettes créées par l'utilisateur sont affichées. En dessous sont affichés les recettes de utilisateurs que cet utilisateur suit.

Un bouton "+" après les recettes permet de créer une nouvelle recette.

### Page Recipe

Sur cette page, toutes les informations d'une recette sont présentes.

Il est possible de noté la recette à l'aide des 5 étoiles.

### Page Create new recipe

Cette page permet de créer un recette.

Les différents champs permettent de donner les différentes informations pour la recette. Les boutons "Add ingredient", "Add ingredients category" et "Add step" permettent d'ajouter des champs. S'il y a une erreur, les bordures du champ en question deviennent rouge.

Pour qu'une recette soit valide il faut:

- Un nom
- Un temps de préparation en minutes.
- Le nombre de personne qui correspondent aux quantités indiquées.
- Aux moins une catégorie d'ingrédient
- Aux moins un ingrédient (une quantité et un nom).
- Aux moins une étape.

Sont facultatif:

- Une image.
- Une description.
- Les tags (5 aux maximum).

### Page 404

Cette page s'affiche en cas d'erreur. Il est possible de revenir au menu (la page de l'utilisateur si ce dernier est connecté sinon la page de connexion) en appuyant sur le lien ou l'image Cookbook juste au dessus de ce dernier.

## Auto-évaluation

### Allemand Adrien

-

### Loyse Krug

Pour ce projet, je me suis concentrée sur la partie backend, en particulier les requêtes avec GraphQL et la communication avec la base de données. 

J'ai passé le début du projet à mettre en place des tests unitaires, puis me suis concentrée sur l'apprentissage de GraphQl côté serveur. Les difficultés principales que j'ai rencontrées se trouvent dans les multiples manière de pouvoir implémenter des requêtes graphQL. J'ai voulu implémenter graphQL côté serveur en utilisant.

### Nanchen Lionel

J'ai passé l'intégralité du projet à travailler sur la partie front-end. L'UX a principalement été créer pas mes soins. Mon manque de connaissances en HTML et CSS explique que l'application n'est pas responsive. Après la création de l'UX, j'ai aidé Olivier à implémenter la communication du front-end avec le back-end.

Au commencement du projet j'ai dû approfondir mes connaissances en Javascript, React, HTML, CSS ce qui a retardé et compliqué mon avancée sur le projet. Concernant le projet, j'étais très motivé par ce dernier. À cause de malheureux problèmes en interne (au groupe) mais aussi à la capacité de l'HEIG-VD à nous proposer un plan semestriel bien trop chargé, nous n'avions pas eu le temps de finir le projet comme nous l'avions pensé. Se rendre compte que l'application contient simplement "le minimum vital" (au lieu d'une application conséquente) est extrêmement frustrant.

### Nicole soit Nicoulaz Olivier

Dès le début de ce projet, nous avons posé une bonne base concernant la structure des fichiers, ce qui m'as permis de directement faire le déploiement automatique de GitLab sur Heroku. J'ai eu quelques soucis de confiuration au départ mais le déploiement était relativement vite prêt pour notre projet.

Ensuite j'ai travaillé sur la partie backend (celle que je préfère personnellement) et qui a occupé la majeur partie de mon temps. Ensuite j'ai travaillé en collaboration avec mes collègues afin de faire la passerelle entre la partie frontend en React et la partie API / GraphQL.

Ce qui m'a demandé le plus de temps était de comprendre le fonctionnement des nouvelles technologies que l'on a utilisé dans ce projet. Je n'avais personnellement jamais utilisé MongoDB et n'avais que très peu de connaissances en React. J'ai aprécié travailler avec ces nouvelles technologies car cela m'a permis de relever un nouveau défis. J'ai par contre moins aprécié le peu de temps que l'on a pû consacrer à ce projet à cause de la surcharge de travailler durant le semestre.

Pour finir, je pense avoir fourni un bon travail mais pas parfait car n'ayant pas le temps de m'intéresser complétement aux technologies utilisées pour ce projet. Je tiens quand même à remercier les professeurs et assistants qui ont été très disponibles pendant et hors de cours pour répondre à mes nombreuses questions.

## Tests

Au début du projet nous avions choisi de baser notre communication entre Client et Serveur et Base de Données sur une API REST. Nous avons ainsi commencé à implémenter des tests simulant de le comportement de postman pour chercher et créer des objets dans la base de données. 

La structure de ces tests se trouve toujours dans notre projet dans le dossier `api/test/` . Malheureusement, le système de communication ayant été entiérement modifié, ces tests ne fonctionnent plus. 

De plus, pris par le temps nous n'avons pas réussi à implémenter des tests liés à l'API de GraphQL.

## Bonus

### Déploiement automatique

Dès le début du projet, nous nous sommes directement intéressés au déploiement automatique fourni par GitLab afin d'avoir notre projet entièrement en ligne sur Heroku lors de chaque commit sur notre branche master. Suite à de nombreux essais, nous avons réussi à effectuer ce déploiement. 

Le pipeline de déploiement se déroule en deux étapes car Heroku n'accepte pas que l'oin déploie plusieurs projets en même temps avec un compte gratuit.

![](images/deploiement.png)

Voici notre fichier de configuration **.gitlab-ci.yml**

   - ```yaml
        image: node:latest
        
        stages:
          - productionFirst
          - productionSecond
          
        APIHeroku:
          type: deploy
          stage: productionFirst
          image: ruby:latest
          script:
            - apt-get update -qy
            - apt-get install -y ruby-dev
            - gem install dpl
            - cd api
            - dpl --provider=heroku --app=cookbook-heig-api --api-key=$HEROKU_API_KEY
          only:
            - master
            
        ClientHeroku:
          type: deploy
          stage: productionSecond
          image: ruby:latest
          script:
            - apt-get update -qy
            - apt-get install -y ruby-dev
            - gem install dpl
            - cd client
            - dpl --provider=heroku --app=cookbook-heig-client --api-key=$HEROKU_API_KEY
          only:
            - master
        
        ```

Ce point nous a permis de tester notre projet en ligne durant toute la phase de développement.

### UX

L'image qui apparait plusieurs fois en fond (l'image des crêpes), le logo et l'icon Cookbook ont été complètement crées par nos soins.

Nous avons ajouté un favicon pour la barre URL et les favoris.

##Problèmes restants

### Passwords non hachés

Les mots de passes stockés en base de données ne sont pas hachés. Nous ne l'avons pas intégré en début de projet et avons décidé de consacrer notre temps à d'autres fonctionnalités du projet. Ce point n'étant pas essentiel car le projet n'est pas mis en réelle production.

### Manque de fonctionnalités dans les recherches avancées

Pris par le temps avec les projets et tests de fin d'année, nous n'avons pas eu le temps d'implémenter des recherches avancées tenant compte des intêtets communs entre utilisateurs. 

### Tests manquants

Nous avons créer des tests pour une API REST, mais avec le changement d'orientation, nous n'avons pas réussi à implémenter de nouveaux tests pour  GraphQL

###Upload d'image

Vu que nous avons utilisé Heroku pour le déploiement et comme ce dernier ne support pas l'upload d'image, nous n'avons pas pu faire cette fonctionnalité. Par contre autant du coté client le support des images des recettes et des avatars des utilisateurs sont implémentés et supportés.

### Affichage de la note de l'utilisateur pour une recette

Normalement dès qu'un utilisateur consulte une recette qu'il a déjà noté, sa note doit apparaitre sur les 5 étoiles. Les étoiles doivent aussi changer si l'utilisateur modifie sa note.

### Redirections

Nous avons un soucis lors des redirection sur deux pages, dès que nous les activons certaines requêtes ne sont pas exécutées. Ces problèmes sont sur les pages Sign up et Create recipe. Nous les avons désactivé les redirections sur la version en ligne du site afin qu'elle soit utilisable. De ce fait l'utilisateur doit changer de page manuellement après un sign up ou une création de recette.

