# MAC TWEB - Social Network

Authors:

- Allemand Adrien
- Krug Loyse
- Nanchen Lionel
- Nicole soit Nicoulaz Olivier

## What it is

Our project is to create a cooking recipes sharing social network. 

####A user :

- can create a profile/ delete his/her profile
- can sign up to access her/his recipes and the community's 
- has a personnal page containing profile informations
- has a wall on which she/he can share her/his recipes. The recipes can be placed inside a recipes book.
- can search for other users and send friend requests
- can manage friend requests
- has access to her/his friends recipes and to all the recipes within a community access. 
- can add a recipe to her/his favorites
- can add a rating/review/comment to a recipe
- can suggest a recipe to a friend
- can seach for recipes with key words (ingredients / title / author). The recipes can be ordered alphabetically, by rankings, by creation dates ...
- can filter recipes, searching for a certain labels/categories
- will receive notifications

####A recipe:

- has a title
- has a desctiption
- can have pictures
- has a liste of ingredients
- has preparation steps
- has a list of labels (veggetarian/ pork/ contains milk)
- has informations about its sharing (friends/community/personal)
- has a global rating (number of stars)
- has a list of comments/ reviews
- has a creation date /update date

#### A cook book:

- has a title
- has a description
- can have a picture
- has (at least) one recipe

## Front-end

- React

## Back-end

- MongoDB
- Node.js

## Communication between frontend and backend

- REST API
- GraphQL

## Technologies

- Bootstrap

## Hosting services

- Heroku

