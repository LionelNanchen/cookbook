# Cook Book - Features

##Login page

- Login with a username <span style="color:red">or email</span> and a password
- Forget my password button.

## General

- All page will display
  - a button to create a new recipe
  - a search bar
  - a button to access the user profile page
  - <span style="color:red">a button to see the Favourites</span>
  - a notification button

## Wall

- Display all recent activities from following users.
- Informations that will be displayed:
  - New cook book
  - New recipe

## Search

- Food search
  - The elements retrieved have a name or label that match the query.
- User search
  - If the search begin with a `@`, users matching the query will be retrieved.
- Filter search: the application will propose several filters
  - Only recipes/ cook books
  - Best rated
  - Most rated/ popular
  - <span style="color:red">By followed user</span>

## User

- Display user's informations:
  - Profile picture
  - User name
  - <span style="color:red">Country, city</span>
  - Number of recipe created
  - Number of cook book created
  - <span style="color:red">Followers</span>
  - <span style="color:red">Following</span>
  - <span style="color:red">Favourites</span>
- Follow/ unfollow button
- Cook book list:
  - if a recipe has no cook book, it is automatically assigned to a "Default" cook book.
- Favourites cook book list:
- If it is the owner profile
  - Settings button
    - Change profile picture
    - Change email
    - <span style="color:red">Notification settings</span>
  - New cook book button

## Notification

- The notification is represented by a bell on the top right of the screen.
- The user is notified when:
  - A user add a rating/ review/ to one of your recipe.

## Cook book

- Cook book informations

  - Picture
  - Cook book's Chef
  - Number of recipe
  - Rating

- Description

- General review of the cook book

  1. Users simply rate the cook book ?

  2. Average from recipes' ratings ?

- Display all the recipes

##Recipe

- Recipe informations:
  - Picture
  - Recipe's Chef
  - Estimated cooking time
  - Rating
  - list of labels
- Description
- List of ingredients
- Steps

## Recipe book creation

- Choose:
  - a picture (optional)
  - a name
  - a description (optional)
  - the recipes

## Recipe creation

- Choose:
  - a cook book
  - a picture (optional)
  - a name
  - a description (optional)
  - labels (optional)
  - list of ingredients
  - steps

## <span style="color:red">The Favourites</span>

- A page that display all favorites cook books and recipes

