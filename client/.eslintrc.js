module.exports = {
  extends: 'airbnb',
  rules: {
    'react/forbid-prop-types': 0,
    'react/jsx-filename-extension': 0,
    'react/require-default-props': 0,
    'array-callback-return': 0,
    'no-undef': 0,
    'react/destructuring-assignment': 0,
    'no-param-reassign': 0,
    'prefer-destructurin': 0,
    'no-restricted-globals': 0,
    'max-len': 0,
    'no-plusplus': 0,
    'react/prefer-stateless-function': 0,
    'react/no-access-state-in-setstate': 0,
    'prefer-destructuring': 0,
    'class-methods-use-this': 0,
    'react/jsx-curly-brace-presence': 0
  },
};
