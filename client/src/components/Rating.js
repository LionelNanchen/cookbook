import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Ratings from 'react-ratings-declarative';
import theme from '../util/Theme';

const { apiURI } = require('../config');

/*
 * render the 5 stars
 */
function renderStars() {
  const stars = [];
  for (let i = 0; i < 5; ++i) {
    stars.push(<Ratings.Widget key={i.toString()} widgetDimension="25px" widgetSpacing="2px" />);
  }
  return stars;
}

/*
 * component that represent the rating stars
 */
export default class Rating extends Component {
  constructor(props) {
    super(props);
    this.changeRating = this.changeRating.bind(this);
  }

  /*
   * handle the modification of the rating
   * newRating - the new rating
   */
  changeRating(newRating) {
    const query = `mutation{
      add_feedback(recipe: "${this.props.recipeId}", feedback: {
        author: "${this.props.user.id}",
        grade: ${newRating},
      }){
        id
      }
    }`;

    fetch(`${apiURI}/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        query,
      }),
    })
      .then(r => r.json())
      .then(window.location.reload());
  }

  render() {
    return (
      <Ratings
        rating={this.props.rating}
        widgetRatedColors={theme.palette.primary.main}
        widgetHoverColors={theme.palette.primary.light}
        changeRating={this.changeRating}
      >
        {renderStars()}
      </Ratings>
    );
  }
}

Rating.propTypes = {
  rating: PropTypes.number.isRequired,
  recipeId: PropTypes.string,
  user: PropTypes.object,
};
