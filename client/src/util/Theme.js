import { createMuiTheme } from '@material-ui/core/styles';

import red from '@material-ui/core/colors/red';
import orange from '@material-ui/core/colors/orange';
import lightGreen from '@material-ui/core/colors/lightGreen';
import grey from '@material-ui/core/colors/grey';

const theme = createMuiTheme({
  palette: {
    primary: {
      lighter: lightGreen[100],
      light: lightGreen[300],
      main: lightGreen[500],
    },

    secondary: {
      lighter: orange[50],
      light: orange[300],
      main: orange[500],
    },

    background: {
      light: grey[50],
      main: grey[200],
      dark: grey[400],
    },

    element: {
      light: grey[400],
      main: grey[500],
      dark: grey[700],
    },

    error: {
      main: red[500],
    },
  },

  typography: {
    useNextVariants: true,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  fontSize: '16px',
});

export default theme;
