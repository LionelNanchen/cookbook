import React, { Component } from 'react';
import axios from 'axios';

const { apiURI } = require('./config');

const {
  Provider: AuthContextProvider,
  Consumer: AuthContext,
} = React.createContext();

class AuthProvider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      user: JSON.parse(window.localStorage.getItem('user')),
      error: null,
      signIn: this.signIn,
      signOut: this.signOut,
    }
  }

  componentDidMount() {
    const token = window.localStorage.getItem('token');
    if (token) {
      axios.get(`${apiURI}/auth/me`, {
        headers: {
          Authorization: `bearer ${token}`,
        }
      })
        .then(response => {
          const { user } = response.data;
          window.localStorage.setItem('user', JSON.stringify(user));
          this.setState({ user });
        })
        .catch(err => {
          localStorage.removeItem('token');
          localStorage.removeItem('user');
        })
    }
  }

  signIn = ({ username, password }) => {
    axios.post(`${apiURI}/auth/login`, { username, password })
      .then(response => {
        const { user, token } = response.data;        // get the user object and the token from the response
        window.localStorage.setItem('token', token);  // save the token in the client browser
        window.localStorage.setItem('user', JSON.stringify(user));  // save the token in the client browser
        this.setState({ user });                      
      })
      .catch(error => {
        this.setState({ error: 'Invalid username or password' });
      })
  }

  signOut = () => {
    localStorage.removeItem('token'); // remove the token from the client browser
    localStorage.removeItem('user');
    window.location.reload();         //reload the page
  }

  render() {
    const { children } = this.props
    return (
      <AuthContextProvider value={this.state}>
        {children}
      </AuthContextProvider>
    )
  }
}

export { AuthContext };
export default AuthProvider;