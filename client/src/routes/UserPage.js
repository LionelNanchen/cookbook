import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import Grid from '@material-ui/core/Grid';
import InputAdornment from '@material-ui/core/InputAdornment';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPlusCircle,
  faCog,
  faAt,
  faHeart,
} from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartEmpty } from '@fortawesome/free-regular-svg-icons';
import PasswordField from '../components/PasswordField';
import UserAvatar from '../components/UserAvatar';
import Navigator from '../components/Navigator';
import Thumbnail from '../components/Thumbnail';
import Util from '../util/Util';
import theme from '../util/Theme';
import userIcon from '../assets/images/user_icon.png';

const { apiURI } = require('../config');

const styles = {
  background: {
    backgroundColor: '#F0F0F0',
    width: '100%',
    minHeight: '100vh',
    height: '100%',
    position: 'relative',
    marginTop: '70px',
  },

  content: {
    position: 'relative',
    width: '90%',
    top: '0px',
    left: '50%',
    transform: 'translateX(-50%)',
  },

  userInfos: {
    height: '180px',
    display: 'flex',
  },

  userInfosAvatar: {
    width: '35%',
  },

  avatar: {
    top: '50%',
    left: '50%',
    transform: 'translateY(-50%) translateX(-50%)',
  },

  username: {
    fontSize: '25px',
    maxWidth: '70%',
    wordWrap: 'break-word',
  },

  userHeader: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: '10px',
  },

  settings: {
    display: 'inline-block',
    width: '50px',
    marginLeft: '10px',
  },

  settingsIcon: {
    color: '#f69454',
    height: '100%',
    width: '100%',
  },

  userInfosText: {
    width: '70%',
  },

  userInfoTextBorder: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  arrayText: {
    fontSize: '1em',
    marginLeft: '10px',
  },

  bold: {
    fontWeight: 'bold',
    textAlign: 'right',
    fontSize: '1em',
  },

  noRecipeText: {
    width: '100%',
    textAlign: 'center',
    marginTop: '20px',
    marginBottom: '20px',
    color: theme.palette.element.main,
  },

  library: {
    marginTop: '20px',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  plus: {
    textAlign: 'center',
    width: '20%',
    minWidth: '200px',
    margin: '10px',
    display: 'inline-block',
    position: 'relative',
  },

  plusButton: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  plusIcon: {
    color: theme.palette.primary.main,
  },

  modal: {
    position: 'absolute',
    backgroundColor: '#F0F0F0',
    padding: '20px',
    outline: 'none',
    textAlign: 'center',
  },

  modalStyle: {
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '2px',
  },

  modalButtons: {
    display: 'block',
    width: '100%',
    textAlign: 'center',
  },

  horizontalLine: {
    borderWidth: '1px',
  },

  changeButtons: {
    display: 'block',
    position: 'relative',
    left: '50%',
    transform: 'translateX(-50%)',
    marginBottom: '10px',
  },

  passwordFields: {
    marginTop: '10px',
    marginBottom: '10px',
  },

  newEmailAddress: {
    marginTop: '10px',
    marginBottom: '10px',
    height: '40px',
  },

  recipeAvatar: {
    height: '300px',
    width: '300px',
  },

  avatarBorder: {
    height: '200px',
    width: '200px',
    border: `1px dotted${theme.palette.element.main}`,
    borderRadius: '50%',
    textAlign: 'center',
    position: 'relative',
    marginTop: '10px',
  },

  avatarButton: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  newAvatar: {
    width: '200px',
    height: '200px',
    borderRadius: '50%',
  },

  followedName: {
    fontSize: '25px',
    marginBottom: '10px',
  },

  // those three are for the green TextField's border
  cssFocused: {},
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: theme.palette.secondary.main,
    },
  },
  notchedOutline: {},
};

/*
 * component that represent the user page
 */
class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false, // if the option modal is open
      modalState: 0, // the state of the modal (to choose wich version to display)
      userAvatar: null, // the user avatar
      // userAvatar: props.user.avatar,
      newPassword: '', // the new password
      newEmail: '', // the new password repeated
      newRepeatPassword: '',
      passwordState: 0, // if the password input is not valid
      emailState: 0, // if the email input is not valid
      user: props.user,
      myAccount: true,
      currentUser: false,
      follow: this.isFollow(), // if the user follow this user
    };
    this.picture = !this.state.user.picture ? userIcon : this.state.user.picture;
    this.handleChooseAvatar = this.handleChooseAvatar.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleAddRecipe = this.handleAddRecipe.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeEmailAddress = this.handleChangeEmailAddress.bind(this);
    this.handleFollow = this.handleFollow.bind(this);
    this.renderPasswordMessage = this.renderPasswordMessage.bind(this);
    this.renderEmailMessage = this.renderEmailMessage.bind(this);
    this.renderFollowedRecipes = this.renderFollowedRecipes.bind(this);
  }

  componentWillMount() {
    const userId = this.props.match.params.userid;
    if (userId && userId !== this.state.user.id) {
      const query = `query{
        user(id: "${userId}"){
          id
          name
          email
          picture
          recipes {
            id
            chief {
              id
              name
            }
            image
            name
            preparationTime
            description
            yield
            ingredientCategories {
              category
              ingredients {
                name
                quantity
              }
            }
            steps {
              stepNumber
              description
            }
            tags
          }
          followers {
            name
          }
          followed {
            name
          }
          notifications {
            text
            path
          }
        }
      }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(r => r.json())
        .then((data) => {
          this.setState({ currentUser: this.state.user });

          // this line will delete the recipes array to fix a bug where recipes from the connected user where shown on another user page
          this.setState({ user: { ...this.state.user, recipes: [] } });
          this.setState({ user: data.data.user, myAccount: false });
        });
    }
  }

  /*
   * executed when the component is mounted
   */
  componentDidMount() {
    document.title = 'Cookbook - User page';
  }

  /*
  * check if the current user follow the displayed user
  */
  isFollow() {
    const userId = this.props.match.params.userid;
    return userId && this.props.user.followed.some(followed => followed.id === userId);
  }

  /*
   * handle the choice for the user avatar
   * event - the button action's event
   */
  handleChooseAvatar(event) {
    this.setState(
      {
        userAvatar: event.target.files[0],
      },
      () => {
        // save the avatar !!!
      },
    );
  }

  /*
   * open the modal
   */
  handleOpenModal() {
    this.setState({ modalIsOpen: true });
  }

  /*
   * close the modal
   */
  handleCloseModal() {
    this.setState({
      modalIsOpen: false,
      modalState: true,
      passwordState: 0,
      emailState: 0,
      newPassword: '',
      newEmail: '',
    });
  }

  /*
   * handle the addition of a new recipe
   */
  handleAddRecipe() {
    window.location.href = '/createrecipe';
  }

  /*
   * handle the change of the value of an input
   * name - the name of the input reference in the state
   * event - the button action's event
   */
  handleChange(name, event) {
    this.setState({
      [name]: event.target.value,
      [`${name}NotValid`]: false,
    });
  }

  /*
   * control the password's inputs and change the password
   */
  handleChangePassword() {
    if (this.state.newPassword === '' || this.state.newRepeatPassword === '') {
      this.setState({
        passwordState: 1,
      });
    } else if (this.state.newPassword !== this.state.newRepeatPassword) {
      this.setState({
        passwordState: 2,
      });
    } else {
      this.setState({
        passwordState: 3,
      });

      const query = `mutation{
        update_user_password(user:"${this.state.user.id}", password:"${
  this.state.newPassword
}"){
          id
        }
      }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      });
    }
  }

  /*
   * control the email's inputs and change the password
   */
  handleChangeEmailAddress() {
    if (this.state.newEmail === '') {
      this.setState({
        emailState: 1,
      });
    } else if (!Util.checkIsValidEmail(this.state.newEmail)) {
      this.setState({
        emailState: 2,
      });
    } else {
      this.setState({
        emailState: 3,
      });
      const query = `mutation{
        update_user_email(user:"${this.state.user.id}", email:"${
  this.state.newEmail
}"){
          id
        }
      }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      });
    }
  }

  /*
   * handle the follow state
   */
  handleFollow() {
    const call = !this.state.follow
      ? 'mutation{ follow_user'
      : 'mutation{ unfollow_user';

    const query = `${call}(user:"${
      this.state.currentUser.id
    }", followed_user:"${this.state.user.id}"){
        followed{
          id
          name
        }
        followers{
          id
          name
        }
      }
    }`;

    fetch(`${apiURI}/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        query,
      }),
    })
      .then(r => r.json())
      .then(this.setState({
        follow: !this.state.follow,
      }));
  }

  /*
   * render the error or success message when changing password
   */
  renderPasswordMessage() {
    switch (this.state.passwordState) {
      default:
        return null;
      case 1:
        return 'The fields are not filled.';
      case 2:
        return "The passwords don't match";
      case 3:
        return 'Your password as successfully been changed';
    }
  }

  /*
   * render the error or success message when changing the email
   */
  renderEmailMessage() {
    switch (this.state.emailState) {
      default:
        return null;
      case 1:
        return 'The fields are not filled.';
      case 2:
        return 'The is not a valid email';
      case 3:
        return 'Your email as successfully been changed';
    }
  }

  /*
   * render the recipes of followed users
   */
  renderFollowedRecipes() {
    const { classes } = this.props;
    if (this.state.myAccount) {
      return (
        <div>
          <hr className={classes.horizontalLine} />
          {this.state.user.followed.length === 0 ? (
            <Typography variant="display1" component="h2" className={classes.noRecipeText}>{"You don't follow any user yet."}</Typography>
          ) : (
            this.state.user.followed.map(followed => (
              <div>
                <Typography className={classes.followedName}>
                  {followed.name}
                </Typography>
                <div className={classes.library}>
                  {followed.recipes.map(recipe => (
                    <Thumbnail recipe={recipe} />
                  ))}
                </div>
              </div>
            ))
          )}
        </div>
      );
    }
    return null;
  }

  /*
   * render the modal
   */
  renderModal() {
    const { classes } = this.props;
    let content = null;
    switch (this.state.modalState) {
      // the modal menu
      default:
        content = (
          <div style={styles.modalStyle} className={classes.modal}>
            <Typography variant="h6">Options</Typography>
            <hr classeName={classes.horizontalLine} />
            <Button
              color="secondary"
              className={classes.modalButtons}
              onClick={() => this.setState({ modalState: 1 })}
            >
              Change avatar
            </Button>
            <Button
              color="secondary"
              className={classes.modalButtons}
              onClick={() => this.setState({ modalState: 2 })}
            >
              Change password
            </Button>
            <Button
              color="secondary"
              className={classes.modalButtons}
              onClick={() => this.setState({ modalState: 3 })}
            >
              Change email address
            </Button>
          </div>
        );
        break;
      // to change avatar
      case 1:
        content = (
          <div style={styles.modalStyle} className={classes.modal}>
            <Typography variant="h6">Change avatar</Typography>
            <hr classeName={classes.horizontalLine} />
            <div className={classes.avatarBorder}>
              {!this.state.userAvatar ? (
                <Button
                  color="secondary"
                  component="label"
                  className={classes.avatarButton}
                >
                  <input
                    type="file"
                    hidden
                    onChange={event => this.handleChooseAvatar(event)}
                  />
                  Choose an avatar
                </Button>
              ) : (
                <Grid component="label">
                  <input
                    type="file"
                    hidden
                    onChange={event => this.handleChooseAvatar(event)}
                  />
                  <UserAvatar
                    className={classes.newAvatar}
                    avatar={URL.createObjectURL(this.state.userAvatar)}
                  />
                  {/* URL.createObjectURL(this.state.userAvatar) */}
                </Grid>
              )}
            </div>
          </div>
        );
        break;
      // to change the password
      case 2:
        content = (
          <div style={styles.modalStyle} className={classes.modal}>
            <Typography variant="h6">Change password</Typography>
            <hr classeName={classes.horizontalLine} />
            <PasswordField
              placeholder="New password"
              className={classes.passwordFields}
              value={this.state.newPassword}
              onChange={event => this.handleChange('newPassword', event)}
            />
            <PasswordField
              placeholder="Repeat password"
              className={classes.passwordFields}
              value={this.state.newRepeatPassword}
              onChange={event => this.handleChange('newRepeatPassword', event)}
            />
            <Button
              variant="contained"
              className={classes.changeButtons}
              onClick={this.handleChangePassword}
            >
              Change password
            </Button>
            <Typography
              color={this.state.passwordState === 3 ? 'primary' : 'error'}
            >
              {this.renderPasswordMessage()}
            </Typography>
          </div>
        );
        break;
      // to change the email address
      case 3:
        content = (
          <div style={styles.modalStyle} className={classes.modal}>
            <Typography variant="h6">Change email address</Typography>
            <hr classeName={classes.horizontalLine} />
            <TextField
              placeholder="New email address"
              variant="outlined"
              value={this.state.newEmail}
              onChange={event => this.handleChange('newEmail', event)}
              className={classes.newEmailAddress}
              InputProps={{
                classes: {
                  root: classes.cssOutlinedInput,
                  focused: classes.cssFocused,
                  notchedOutline: classes.notchedOutline,
                },
                startAdornment: (
                  <InputAdornment position="start">
                    <FontAwesomeIcon className={classes.icon} icon={faAt} />
                  </InputAdornment>
                ),
              }}
            />
            <Button
              variant="contained"
              className={classes.changeButtons}
              onClick={this.handleChangeEmailAddress}
            >
              Change email address
            </Button>
            <Typography
              color={this.state.emailState === 3 ? 'primary' : 'error'}
            >
              {this.renderEmailMessage()}
            </Typography>
          </div>
        );
        break;
    }
    return content;
  }

  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <Navigator user={this.state.user} />
        <main className={classes.background}>
          <div className={classes.content}>
            <div className={classes.userInfos}>
              <div className={classes.userInfosAvatar}>
                <UserAvatar
                  avatar={this.picture}
                  className={classes.avatar}
                />
              </div>
              <div className={classes.userInfosText}>
                <div className={classes.userInfoTextBorder}>
                  <div className={classes.userHeader}>
                    <Typography className={classes.username}>
                      {this.state.user.name}
                    </Typography>
                    {this.state.myAccount ? (
                      <IconButton
                        color="secondary"
                        className={classes.settings}
                        onClick={this.handleOpenModal}
                      >
                        <FontAwesomeIcon
                          icon={faCog}
                          className={classes.settingsIcon}
                        />
                      </IconButton>
                    ) : (
                      <IconButton
                        color="secondary"
                        className={classes.settings}
                        onClick={this.handleFollow}
                      >
                        {this.state.follow ? (
                          <FontAwesomeIcon
                            icon={faHeart}
                            className={classes.settingsIcon}
                          />
                        ) : (
                          <FontAwesomeIcon
                            icon={faHeartEmpty}
                            className={classes.settingsIcon}
                          />
                        )}
                      </IconButton>
                    )}
                  </div>
                  <tr>
                    <td>
                      <Typography className={classes.bold}>
                        {this.state.user.recipes.length || 0}
                      </Typography>
                    </td>
                    <td>
                      <Typography className={classes.arrayText}>
                        recipe
                        {Util.plural(this.state.user.recipes.length || 0)}
                      </Typography>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Typography className={classes.bold}>
                        {this.state.user.followers.length || 0}
                      </Typography>
                    </td>
                    <td>
                      <Typography className={classes.arrayText}>
                        follower
                        {Util.plural(this.state.user.recipes.length || 0)}
                      </Typography>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <Typography className={classes.bold}>
                        {this.state.user.followed.length || 0}
                      </Typography>
                    </td>
                    <td>
                      <Typography className={classes.arrayText}>
                        followed
                      </Typography>
                    </td>
                  </tr>
                </div>
              </div>
            </div>
            <div className={classes.library}>
              {Object.keys(this.state.user.recipes).length === 0 ? (
                <Typography
                  className={classes.noRecipeText}
                  component="h2"
                  variant="display1"
                >
                  {"You don't have any recipe yet."}
                </Typography>
              ) : (
                this.state.user.recipes.map(recipe => (
                  <Thumbnail recipe={recipe} />
                ))
              )}
              {this.state.myAccount ? (
                <div className={classes.plus}>
                  <IconButton
                    color="primary"
                    className={classes.plusButton}
                    onClick={this.handleAddRecipe}
                  >
                    <FontAwesomeIcon
                      icon={faPlusCircle}
                      className={classes.plusIcon}
                      size="2x"
                    />
                  </IconButton>
                </div>
              ) : null}
            </div>
            {this.renderFollowedRecipes()}
          </div>
          <Modal open={this.state.modalIsOpen} onClose={this.handleCloseModal}>
            {this.renderModal()}
          </Modal>
        </main>
      </MuiThemeProvider>
    );
  }
}

UserPage.propTypes = {
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  match: PropTypes.object,
};

export default withStyles(styles)(UserPage);
