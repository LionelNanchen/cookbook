import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import Thumbnail from '../components/Thumbnail';
import Navigator from '../components/Navigator';
import Util from '../util/Util';
import theme from '../util/Theme';

const stringifyObject = require('stringify-object');
const { apiURI } = require('../config');

const styles = {
  background: {
    backgroundColor: '#F0F0F0',
    width: '100%',
    position: 'relative',
    marginTop: '70px',
    minHeight: '100vh',
  },

  content: {
    position: 'relative',
    width: '90%',
    top: '0px',
    left: '50%',
    transform: 'translateX(-50%)',
  },

  options: {
    height: '100px',
  },

  choosedFiltersList: {
    display: 'inline-block',
    paddingLeft: '10px',
  },

  li: {
    display: 'inline-block',
    borderRight: '1px solid #000',
    margin: '0px 0px 0px 8px',
    paddingRight: '8px',
    '&:last-child': {
      borderRight: '0px',
    },
  },

  crossIconButton: {
    height: '8px',
    width: '8px',
  },

  crossIcon: {
    height: '12px',
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  filters: {
    display: 'flex',
  },

  filterButtonBorder: {
    height: '30px',
  },

  filterButton: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  filterList: {
    zIndex: '10',
  },

  filterListItem: {
    backgroundColor: theme.palette.background.light,
    border: '1px solid',
    borderColor: theme.palette.background.main,
    '&:hover': {
      backgroundColor: theme.palette.background.dark,
    },
  },

  horizontalLine: {
    borderWidth: '1px',
  },

  resultFoundText: {
    display: 'inline',
    fontSize: '20px',
  },

  resultFoundBold: {
    display: 'inline',
    fontWeight: 'bold',
    fontSize: '20px',
  },

  library: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
};

/*
 * component that represent the search page
 */
class SearchPage extends Component {
  constructor(props) {
    super(props);

    const urlParams = new URLSearchParams(window.location.search);
    const searchTerm = urlParams.get('search');

    this.state = {
      filterOpen: false, // if the filter selector is open
      filters: Util.tags(),
      currentNbFilter: 0, // the current number of choosen filter
      choosedFilters: [], // the choosen filter
      user: props.user,
      recipes: [],
      searchTerm,
    };

    this.handleCollapse = this.handleCollapse.bind(this);
    this.handleChooseFilter = this.handleChooseFilter.bind(this);
    this.handleDeleteFilter = this.handleDeleteFilter.bind(this);
  }

  componentWillMount() {
    if (this.state.searchTerm) {
      const query = `{
        search_recipe(
          name:"${this.state.searchTerm}",
          containedTags:[],
          notContainedTags:[]){
            id
            chief {
              id
              name
            }
            image
            name
            preparationTime
            description
            yield
            ingredientCategories {
              category
              ingredients {
                name
                quantity
              }
            }
            steps {
              stepNumber
              description
            }
            tags
          }
        }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(r => r.json())
        .then(data => this.setState({ recipes: data.data.search_recipe }));
    }
  }

  /*
   * executed when the component is mounted
   */
  componentDidMount() {
    document.title = 'Cookbook - Search';
  }

  /*
   * handle the display of the filter's selector
   */
  handleCollapse() {
    this.setState({ filterOpen: !this.state.filterOpen });
  }

  /*
   * add a filter in the filter list
   * filter - the choosed filter
   */
  handleChooseFilter(filter) {
    if (this.state.currentNbFilter > 4 || filter.taken) return;
    const list = this.state.choosedFilters;
    list.push(filter);
    filter.taken = true;
    this.setState({
      choosedFilters: list,
      currentNbFilter: this.state.currentNbFilter + 1,
    });

    if (this.state.searchTerm) {
      const choosedFilterString = this.state.choosedFilters.map(item => item.name);
      const query = `{
        search_recipe(
          name:"${this.state.searchTerm}",
          containedTags:${stringifyObject(choosedFilterString, { singleQuotes: false })},
          notContainedTags:[]){
            id
            chief {
              id
              name
            }
            image
            name
            preparationTime
            description
            yield
            ingredientCategories {
              category
              ingredients {
                name
                quantity
              }
            }
            steps {
              stepNumber
              description
            }
            tags
          }
        }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(r => r.json())
        .then((data) => {
          this.setState({ recipes: [] });
          this.setState({ recipes: data.data.search_recipe });
        });
    }
  }

  /*
   * remove a filter of the filter list
   * filter - the choosed filter
   */
  handleDeleteFilter(filter) {
    const list = this.state.choosedFilters;
    let i = 0;
    for (; i < list.length; ++i) if (list[i] === filter) break;
    list[i].taken = false;
    list.splice(i, 1);
    this.setState({
      choosedFilters: list,
    });
  }

  render() {
    const { classes } = this.props;

    return (
      <MuiThemeProvider theme={theme}>
        <Navigator user={this.state.user} />
        <main className={classes.background}>
          <div className={classes.content}>
            <div className={classes.options}>
              <div>
                <Typography>
                  Choosed Filters:
                  <ul className={classes.choosedFiltersList}>
                    {this.state.choosedFilters.map(filter => (
                      <li className={classes.li}>
                        {filter.name}
                        <IconButton
                          className={classes.crossIconButton}
                          onClick={() => this.handleDeleteFilter(filter)}
                        >
                          <FontAwesomeIcon
                            icon={faTimes}
                            className={classes.crossIcon}
                          />
                        </IconButton>
                      </li>
                    ))}
                  </ul>
                </Typography>
              </div>
              <div className={classes.filters}>
                <List className={classes.filterButtonBorder}>
                  <ListItem
                    button
                    className={classes.filterButton}
                    onClick={this.handleCollapse}
                  >
                    <ListItemText primary="Filter" />
                    {this.state.filterOpen ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={this.state.filterOpen} unmountOnExit>
                    <List className={classes.filterList}>
                      {this.state.filters.map(filter => (
                        <ListItem
                          button
                          className={classes.filterListItem}
                          onClick={() => this.handleChooseFilter(filter)}
                        >
                          <ListItemText primary={filter.name} />
                        </ListItem>
                      ))}
                    </List>
                  </Collapse>
                </List>
              </div>
            </div>
            <hr className={classes.horizontalLine} />
            <div>
              <div>
                <Typography
                  component="h2"
                  variant="display1"
                  className={classes.resultFoundText}
                >
                  {this.state.recipes.length}
                  {' '}
                  result
                  {Util.plural(this.state.recipes.length)}
                  {' '}
                  found for
                  {' '}
                </Typography>
                <Typography
                  component="h2"
                  variant="display1"
                  className={classes.resultFoundBold}
                >
                  {this.state.searchTerm}
                </Typography>
              </div>
              <div className={classes.library}>
                {this.state.recipes.map(recipe => (
                  <Thumbnail recipe={recipe} />
                ))}
              </div>
            </div>
          </div>
        </main>
      </MuiThemeProvider>
    );
  }
}

SearchPage.propTypes = {
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SearchPage);
