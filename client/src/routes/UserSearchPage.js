import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import theme from '../util/Theme';
import Util from '../util/Util';
import Navigator from '../components/Navigator';
import UserThumbnail from '../components/UserThumbnail';

const { apiURI } = require('../config');

const styles = {
  background: {
    backgroundColor: '#F0F0F0',
    width: '100%',
    position: 'relative',
    marginTop: '70px',
    minHeight: '100vh',
    height: '100%',
  },

  content: {
    position: 'relative',
    width: '90%',
    top: '0px',
    left: '50%',
    transform: 'translateX(-50%)',
  },

  library: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },

  resultFound: {
    paddingTop: '10px',
  },

  resultFoundText: {
    display: 'inline',
    fontSize: '20px',
  },

  resultFoundBold: {
    display: 'inline',
    fontWeight: 'bold',
    fontSize: '20px',
  },
};

/*
 * component that represent the user search page
 */
class UserSearchPage extends Component {
  constructor(props) {
    super(props);

    const urlParams = new URLSearchParams(window.location.search);
    const searchTerm = urlParams.get('search');

    this.state = {
      user: props.user,
      users: [],
      searchTerm,
    };
  }

  componentWillMount() {
    if (this.state.searchTerm) {
      const query = `{
        search_user(research:"${this.state.searchTerm}"){
          id
          name
          picture
        }
      }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(r => r.json())
        .then(data => this.setState({ users: data.data.search_user }));
    }
  }

  /*
   * executed when the component is mounted
   */
  componentDidMount() {
    document.title = 'Cookbook - Search';
  }

  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <Navigator user={this.state.user} />
        <main className={classes.background}>
          <div className={classes.content}>
            <div className={classes.resultFound}>
              <Typography
                component="h2"
                variant="display1"
                className={classes.resultFoundText}
              >
                {this.state.users.length}
                {' result'}
                {Util.plural(this.state.users.length)}
                {' found for '}
              </Typography>
              <Typography
                component="h2"
                variant="display1"
                className={classes.resultFoundBold}
              >
                {this.state.searchTerm}
              </Typography>
            </div>
            <div className={classes.library}>
              {this.state.users.map(user => (
                <UserThumbnail user={user} />
              ))}
            </div>
          </div>
        </main>
      </MuiThemeProvider>
    );
  }
}

UserSearchPage.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserSearchPage);
