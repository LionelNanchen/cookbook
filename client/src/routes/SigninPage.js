import React, { useState } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAt } from '@fortawesome/free-solid-svg-icons';
import theme from '../util/Theme';
import SignBackground from '../components/SignBackground';
import PasswordField from '../components/PasswordField';
import { AuthContext } from '../AuthProvider';
import logo from '../assets/images/cookbook_icon.png';

const styles = {
  panel: {
    textAlign: 'center',
    borderRadius: '8px',
    backgroundColor: theme.palette.background.main,
    verticalAlign: 'middle',
    overflow: 'scroll',
    position: 'absolute',
    left: '50%',
    top: '50%',
    transform: 'translateX(-50%) translateY(-50%)',
    width: '400px',
    filter: 'opacity(90%)',
  },

  avatar: {
    margin: 'auto',
    marginTop: '20px',
    marginBottom: '10px',
    width: '72px',
    height: '72px',
  },

  title: {
    margin: '10px 10px',
  },

  inputs: {
    width: '70%',
    height: '40px',
  },

  inputsSignin: {
    marginTop: '20px',
    marginBottom: '10px',
  },

  bottomInputs: {
    width: '70%',
    height: '30px',
    marginBottom: '20px',
  },

  horizontalLine: {
    borderWidth: '1px',
    width: '70%',
  },

  icon: {
    color: theme.palette.element.main,
  },

  error: {
    color: theme.palette.error.main,
  },

  // those three are for the green TextField's border
  cssFocused: {},
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: theme.palette.secondary.main,
    },
  },
  notchedOutline: {},
};

/*
 * component that represent the signin page
 */
const SigninPage = (props) => {
  // react hooks
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const { classes } = props;

  return (
    // error, user and signIn declared in AuthProvider
    <AuthContext>
      {({ error, user, signIn }) => {
        // if the user isn't connected (objet user set) he will be redirected to "/"
        if (user) {
          return <Redirect to="/" />;
        }

        // event on login form submit
        const onSubmit = (e) => {
          e.preventDefault();
          signIn({ username, password });
        };

        return (
          <MuiThemeProvider theme={theme}>
            <div>
              <SignBackground />
              <Paper id="signin-panel" className={classes.panel}>
                <Avatar src={logo} className={classes.avatar} />
                <Typography variant="h4" className={classes.title}>
                  Welcome to Cookbook
                </Typography>
                <form onSubmit={onSubmit}>
                  <TextField
                    value={username}
                    onChange={e => setUsername(e.target.value)}
                    variant="outlined"
                    id="signin-email"
                    className={classes.inputs}
                    placeholder="Email Address"
                    type="email"
                    margin="normal"
                    InputProps={{
                      classes: {
                        root: classes.cssOutlinedInput,
                        focused: classes.cssFocused,
                        notchedOutline: classes.notchedOutline,
                      },
                      startAdornment: (
                        <InputAdornment position="start">
                          <FontAwesomeIcon
                            className={classes.icon}
                            icon={faAt}
                          />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <PasswordField
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    id="signin-password"
                    className={classes.inputs}
                    placeholder="Password"
                  />

                  <div className={classes.inputsSignin}>
                    <Button
                      id="signin-button-signin"
                      variant="contained"
                      className={classes.inputs}
                      type="submit"
                    >
                      Sign in
                    </Button>
                  </div>
                  <p className={classes.error}>{error}</p>
                </form>
                <hr className={classes.horizontalLine} />
                <Button
                  id="signin-button-signin"
                  variant="contained"
                  className={classes.bottomInputs}
                  href="/signup"
                >
                  Sign up
                </Button>
                <Button
                  id="signin-button-forgot-password"
                  variant="contained"
                  className={classes.bottomInputs}
                  href="/forgotpassword"
                >
                  Forgot my password ?
                </Button>
              </Paper>
            </div>
          </MuiThemeProvider>
        );
      }}
    </AuthContext>
  );
};

SigninPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SigninPage);
