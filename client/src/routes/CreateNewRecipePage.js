import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faBookOpen, faCarrot, faStopwatch, faUtensils,
} from '@fortawesome/free-solid-svg-icons';
import UserAvatar from '../components/UserAvatar';
import Navigator from '../components/Navigator';
import theme from '../util/Theme';
import Util from '../util/Util';

const stringifyObject = require('stringify-object');
const { apiURI } = require('../config');

const styles = {
  background: {
    backgroundColor: theme.palette.background.main,
    width: '100%',
    position: 'relative',
    marginTop: '70px',
    minHeight: '100vh',
    height: '100%',
  },

  content: {
    position: 'relative',
    width: '90%',
    top: '0px',
    left: '50%',
    transform: 'translateX(-50%)',
  },

  title: {
    fontSize: '20px',
    paddingTop: '8px',
  },

  horizontalLine: {
    borderWidth: '1px',
  },

  columns: {
    display: 'flex',
  },

  infosColumn: {
    width: '24%',
  },

  ingredientsColumn: {
    width: '38%',
  },

  preparationColumn: {
    width: '38%',
  },

  recipeAvatar: {
    height: '300px',
    width: '300px',
  },

  recipeImageBorder: {
    height: '200px',
    width: '200px',
    border: `1px dotted ${theme.palette.element.main}`,
    textAlign: 'center',
    position: 'relative',
    marginTop: '10px',
  },

  recipeImageButton: {
    position: 'relative',
    top: '50%',
    transform: 'translateY(-50%)',
  },

  recipeImage: {
    width: '200px',
    height: '200px',
    borderRadius: '2px',
  },

  icon: {
    color: theme.palette.element.main,
  },

  ingredients: {
    marginTop: '0px',
  },

  ingredientsCategory: {
    margin: '0px',
  },

  step: {
    marginTop: '10px',
  },

  stepTitle: {
    fontSize: '18px',
  },

  inputs: {
    width: '200px',
  },

  saveButton: {
    width: '200px',
  },

  inputIngredientQuantity: {
    margin: '5px 10px 5px 10px',
    width: '90px',
  },

  inputIngredientName: {
    margin: '0px',
    marginTop: '5px',
  },

  inputStep: {
    margin: '10px',
    width: 'calc(100% - 20px)',
  },

  addIngredientButton: {
    marginLeft: '10px',
    marginTop: '10px',
    textTransform: 'none',
  },

  addIngredientsCategoryButton: {
    marginTop: '10px',
    marginBottom: '10px',
    textTransform: 'none',
    display: 'block',
  },

  addStepButton: {
    textTransform: 'none',
    marginBottom: '10px',
  },
};

/*
 * component that represent the page to create a new recipe
 */
class CreateNewRecipePage extends Component {
  constructor(props) {
    super(props);

    this.tags = Util.tags();

    this.state = {
      user: props.user,
      recipeImage: '', // the image for the recipe
      recipeName: '', // the name of the recipe
      recipeNameNotValid: false, // if the user input for the recipe name is not valid
      preparationTimeNotValid: false, // if the user input for the preparation time is not valid
      yieldNotValid: false, // if the user input for the yield is not valid
      choosenTags: [],
      globalStepIteration: 1, // counter for the number of steps
      steps: [
        // array of steps
        {
          notValid: false, // if the user input is not valid
          stepNumber: 1, // the iteration of this step
          description: '', // the description of the steo
        },
      ],
      ingredientsCategories: [
        {
          // array of category of ingredients
          notValid: false, // if the user input is not valid
          id: 0, // the id of this set
          category: '', // the name of this category
          ingredients: [
            {
              // array of ingredients
              quantityNotValid: false, // if the user input for the quantity is not valid
              nameNotValid: false, // if the user input for the name is not valid
              id: 0, // the if of this ingredient
              quantity: '', // the quantity
              name: '', // the name
            },
          ],
        },
      ],
    };
    this.notifications = props.notifications;
    this.handleChooseRecipeImage = this.handleChooseRecipeImage.bind(this);
    this.handleChooseTag = this.handleChooseTag.bind(this);
    this.addIngredientsCategoryOnClick = this.addIngredientsCategoryOnClick.bind(this);
    this.addIngredientOnClick = this.addIngredientOnClick.bind(this);
    this.addStepOnClick = this.addStepOnClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeIngredientsCategory = this.handleChangeIngredientsCategory.bind(this);
    this.handleChangeIngredientQuantity = this.handleChangeIngredientQuantity.bind(this);
    this.handleChangeIngredientName = this.handleChangeIngredientName.bind(this);
    this.handleChangeStep = this.handleChangeStep.bind(this);
    this.handleSave = this.handleSave.bind(this);
  }

  /*
   * executed when the component is mounted
   */
  componentDidMount() {
    document.title = 'Cookbook - Create a new recipe';
  }

  /*
   * handle the choice for the recipe image
   * event - the button action's event
   */
  handleChooseRecipeImage(event) {
    this.setState({ recipeImage: event.target.files[0] },
      () => {
        // save the image !!!
      });
  }

  /*
   * add another category of ingredients set of inputs
   */
  addIngredientsCategoryOnClick() {
    const list = this.state.ingredientsCategories;
    // if the last category is not filled, no need to another category
    if (list[list.length - 1].category === '') return;
    list.push({
      id: list.length,
      category: '',
      notValid: false,
      ingredients: [
        {
          id: 0,
          quantity: '',
          name: '',
          quantityNotValid: false,
          nameNotValid: false,
        },
      ],
    });
    this.setState({
      ingredientsCategories: list,
    });
  }

  handleChooseTag(event) {
    if (this.state.choosenTags.length >= 5) return;
    this.setState({ choosenTags: event.target.value });
  }

  /*
   * add another ingredient set of inputs
   * categroyId - the id of category of this ingredient
   */
  addIngredientOnClick(categoryId) {
    const list = this.state.ingredientsCategories;

    list.forEach((category) => {
      if (category.id === categoryId) {
        // if the last ingredient is not filled, no need to another ingredient
        if (
          category.ingredients[category.ingredients.length - 1].quantity === ''
          || category.ingredients[category.ingredients.length - 1].name === ''
        ) return;
        category.ingredients.push({
          id: list[categoryId].ingredients.length,
          quantity: '',
          name: '',
          quantityNotValid: false,
          nameNotValid: false,
        });
      }
    });

    this.setState({
      ingredientsCategories: list,
    });
  }

  /*
   * add another step input
   */
  addStepOnClick() {
    const list = this.state.steps;

    // if the last step is not filled, no need to another step
    if (list[list.length - 1].description === '') return;

    list.push({
      stepNumber: this.state.globalStepIteration + 1,
      description: '',
      notValid: false,
    });

    this.setState({
      globalStepIteration: this.state.globalStepIteration + 1,
      steps: list,
    });
  }

  /*
   * handle the change of the value of an input
   * name - the name of the input reference in the state
   * event - the button action's event
   */
  handleChange(name, event) {
    this.setState({
      [name]: event.target.value,
      [`${name}NotValid`]: false,
    });
  }

  /*
   * handle the change of the value of an ingredients category input
   * id - the id of the category of ingredients
   * event - the button action's event
   */
  handleChangeIngredientsCategory(id, event) {
    const list = this.state.ingredientsCategories;
    list[id].category = event.target.value;
    this.setState({
      ingredientsCategories: list,
    });
  }

  /*
   * handle the change of the value of an ingredient quantity input
   * idC - the id of the category of the ingredient
   * id - the id of the category of ingredients
   * event - the button action's event
   */
  handleChangeIngredientQuantity(idC, id, event) {
    const list = this.state.ingredientsCategories;
    list[idC].ingredients[id].quantity = event.target.value;
    this.setState({
      ingredientsCategories: list,
    });
  }

  /*
   * handle the change of the value of an ingredient name input
   * idC - the id of the category of the ingredient
   * id - the id of the category of ingredients
   * event - the button action's event
   */
  handleChangeIngredientName(idC, id, event) {
    const list = this.state.ingredientsCategories;
    list[idC].ingredients[id].name = event.target.value;
    this.setState({
      ingredientsCategories: list,
    });
  }

  /*
   * handle the change of the value of an step input
   * id - the id of the step
   * event - the button action's event
   */
  handleChangeStep(id, event) {
    const list = this.state.steps;
    list[id].description = event.target.value;
    this.setState({
      steps: list,
    });
  }

  /*
   * control the recipe informations and save/ create a new recipe
   * event - the button action's event
   */
  handleSave(event) {
    event.preventDefault();
    let hasError = false;

    if (this.state.recipeName === '') {
      this.setState({
        recipeNameNotValid: true,
      });
      hasError = true;
    }

    if (isNaN(this.state.preparationTime) || this.state.preparationTime <= 0) {
      this.setState({
        preparationTimeNotValid: true,
      });
      hasError = true;
    }

    if (isNaN(this.state.yield) || this.state.yield <= 0) {
      this.setState({
        yieldNotValid: true,
      });
      hasError = true;
    }

    const categories = this.state.ingredientsCategories;
    // if the last category is empty, delete it
    if (
      categories[categories.length - 1].id !== 0
      && categories[categories.length - 1].category === ''
    ) {
      categories.pop();
    }
    categories.map((category) => {
      // if the last ingredient is empty, delete it
      if (
        category.ingredients[category.ingredients.length - 1].id !== 0
        && category.ingredients[category.ingredients.length - 1].quantity === ''
        && category.ingredients[category.ingredients.length - 1].name === ''
      ) {
        category.ingredients.pop();
      }

      if (category.category === '') {
        category.notValid = true;
        hasError = true;
      }
      category.ingredients.map((ingredient) => {
        if (ingredient.quantity === '') {
          ingredient.quantityNotValid = true;
          hasError = true;
        }

        if (ingredient.name === '') {
          ingredient.nameNotValid = true;
          hasError = true;
        }
      });
    });

    const steps = this.state.steps;
    // if the last step is empty, delete it
    if (steps[steps.length - 1].stepNumber !== 1 && steps[steps.length - 1].description === '') {
      steps.pop();
    }
    steps.map((step) => {
      if (step.description === '') {
        step.notValid = true;
        hasError = true;
      }
    });

    this.setState({
      ingredientsCategories: categories,
      steps,
    });

    if (hasError) {
      return;
    }

    // keep only the attributes for the graphQL query
    const stepGraphQL = steps.map((step) => {
      const { notValid, ...data } = step;
      return data;
    });

    const categoriesGraphQL = categories.map((category) => {
      let { ingredients } = category;
      ingredients = ingredients.map((ingredient) => {
        const {
          quantityNotValid, nameNotValid, id, ...dataI
        } = ingredient;
        return dataI;
      });
      category.ingredients = ingredients;
      const { notValid, id, ...data } = category;
      return data;
    });
    const query = `mutation{
      create_recipe(
        chief: "${this.state.user.id}",
        name: "${this.state.recipeName}",
        image: "${this.state.recipeImage}",
        preparationTime: ${this.state.preparationTime},
        description: "${this.state.recipeDescription}",
        yield: ${this.state.yield},
        steps: ${stringifyObject(stepGraphQL, { singleQuotes: false })},
        ingredientCategories: ${stringifyObject(categoriesGraphQL, { singleQuotes: false })},
        tags: ${stringifyObject(this.state.choosenTags, { singleQuotes: false })}
      ){
        name
      }
    }`;

    fetch(`${apiURI}/graphql`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        query,
      }),
    })
      .then(r => r.json())
      .then(); // window.location.href = '/'
  }

  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <Navigator user={this.state.user} />
        <main className={classes.background}>
          <div className={classes.content}>
            <div>
              <Typography component="h2" variant="display1" className={classes.title}>
                Create a new recipe
              </Typography>
            </div>
            <hr style={styles.horizontalLine} />
            <div className={classes.columns}>
              <div className={classes.infosColumn}>
                <Typography className={classes.title}>Recipe informations</Typography>
                <div className={classes.recipeImageBorder}>
                  {!this.state.recipeImage ? (
                    <Button color="primary" component="label" className={classes.recipeImageButton}>
                      <input
                        type="file"
                        hidden
                        onChange={event => this.handleChooseRecipeImage(event)}
                      />
                      Add a recipe image
                    </Button>
                  ) : (
                    <Grid component="label">
                      <input
                        type="file"
                        hidden
                        onChange={event => this.handleChooseRecipeImage(event)}
                      />
                      <UserAvatar
                        className={classes.recipeImage}
                        avatar={this.state.recipeImage}
                      />
                    </Grid>
                  )}
                </div>
                <TextField
                  variant="outlined"
                  className={classes.inputs}
                  placeholder="Recipe name"
                  type="text"
                  margin="normal"
                  error={this.state.recipeNameNotValid}
                  value={this.state.recipeName}
                  onChange={event => this.handleChange('recipeName', event)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <FontAwesomeIcon className={classes.icon} icon={faCarrot} />
                      </InputAdornment>
                    ),
                  }}
                />
                <TextField
                  variant="outlined"
                  className={classes.inputs}
                  placeholder="Recipe description"
                  type="text"
                  margin="normal"
                  multiline
                  value={this.state.recipeDescription}
                  onChange={event => this.handleChange('recipeDescription', event)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <FontAwesomeIcon className={classes.icon} icon={faBookOpen} />
                      </InputAdornment>
                    ),
                  }}
                />
                <TextField
                  variant="outlined"
                  className={classes.inputs}
                  placeholder="Prep. time (min)"
                  type="number"
                  margin="normal"
                  error={this.state.preparationTimeNotValid}
                  value={this.state.preparationTime}
                  onChange={event => this.handleChange('preparationTime', event)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <FontAwesomeIcon className={classes.icon} icon={faStopwatch} />
                      </InputAdornment>
                    ),
                  }}
                />
                <TextField
                  variant="outlined"
                  className={classes.inputs}
                  placeholder="Yield"
                  type="number"
                  margin="normal"
                  error={this.state.yieldNotValid}
                  value={this.state.yield}
                  onChange={event => this.handleChange('yield', event)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <FontAwesomeIcon className={classes.icon} icon={faUtensils} />
                      </InputAdornment>
                    ),
                  }}
                />

                <FormControl className={classNames(classes.tagButton)}>
                  <InputLabel className={classes.tagLabel}>Tag (max 5)</InputLabel>
                  <Select
                    multiple
                    className={classes.inputs}
                    value={this.state.choosenTags}
                    onChange={event => this.handleChooseTag(event)}
                    input={<Input />}
                    renderValue={selected => selected.join(', ')}
                  >
                    {this.tags.map(tag => (
                      <MenuItem key={tag.name} value={tag.name}>
                        <Checkbox
                          color="primary"
                          checked={this.state.choosenTags.indexOf(tag.name) > -1}
                        />
                        <ListItemText primary={tag.name} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>

                <p>
                  <Button
                    variant="contained"
                    className={classes.saveButton}
                    onClick={event => this.handleSave(event)}
                  >
                    Save
                  </Button>
                </p>
              </div>

              <div className={classes.ingredientsColumn}>
                <Typography className={classes.title}>Ingredients</Typography>
                {this.state.ingredientsCategories.map(category => (
                  <div>
                    <div className={classes.ingredientsCategory}>
                      <TextField
                        variant="outlined"
                        className={classes.inputs}
                        placeholder="Ingredients category"
                        error={category.notValid}
                        type="text"
                        margin="normal"
                        value={category.category}
                        onChange={event => this.handleChangeIngredientsCategory(category.id, event)}
                      />
                    </div>
                    {category.ingredients.map(ingredient => (
                      <div className={classes.ingredients}>
                        <TextField
                          variant="outlined"
                          className={classNames(classes.inputs, classes.inputIngredientQuantity)}
                          placeholder="Quantity"
                          type="text"
                          margin="normal"
                          error={ingredient.quantityNotValid}
                          value={ingredient.quantity}
                          onChange={
                            event => this.handleChangeIngredientQuantity(category.id, ingredient.id, event)
                          }
                        />
                        <TextField
                          variant="outlined"
                          className={classNames(classes.inputs, classes.inputIngredientName)}
                          placeholder="Ingredient name"
                          type="text"
                          margin="normal"
                          error={ingredient.nameNotValid}
                          value={ingredient.name}
                          onChange={
                            event => this.handleChangeIngredientName(category.id, ingredient.id, event)
                          }
                        />
                      </div>
                    ))}
                    <Button
                      color="primary"
                      className={classes.addIngredientButton}
                      onClick={() => this.addIngredientOnClick(category.id)}
                    >
                      Add an ingredient
                    </Button>
                  </div>
                ))}
                <Button
                  color="primary"
                  className={classes.addIngredientsCategoryButton}
                  onClick={this.addIngredientsCategoryOnClick}
                >
                  Add an ingredients category
                </Button>
              </div>

              <div className={classes.preparationColumn}>
                <Typography className={classes.title}>Preparation</Typography>
                {this.state.steps.map(step => (
                  <div className={classes.step}>
                    <Typography className={classes.stepTitle}>
                      {`Step ${step.stepNumber}`}
                    </Typography>
                    <TextField
                      variant="outlined"
                      multiline
                      className={classNames(classes.inputs, classes.inputStep)}
                      placeholder="Step description"
                      type="text"
                      margin="normal"
                      error={step.notValid}
                      value={step.description}
                      onChange={event => this.handleChangeStep(step.stepNumber - 1, event)}
                    />
                  </div>
                ))}
                <Button
                  color="primary"
                  className={classes.addStepButton}
                  onClick={this.addStepOnClick}
                >
                  Add a Step
                </Button>
              </div>
            </div>
          </div>
        </main>
      </MuiThemeProvider>
    );
  }
}

CreateNewRecipePage.propTypes = {
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  notifications: PropTypes.array,
};

export default withStyles(styles)(CreateNewRecipePage);
