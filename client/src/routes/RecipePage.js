import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUtensils, faStopwatch } from '@fortawesome/free-solid-svg-icons';
import theme from '../util/Theme';
import Util from '../util/Util';
import Navigator from '../components/Navigator';
import UserAvatar from '../components/UserAvatar';
import Step from '../components/Step';
import Ingredient from '../components/Ingredient';
import Rating from '../components/Rating';
import Link from '../components/Link';
import crepe from '../assets/images/background.jpg';

const { apiURI } = require('../config');

const styles = {
  background: {
    backgroundColor: theme.palette.background.main,
    width: '100%',
    position: 'relative',
    marginTop: '70px',
    minHeight: '100vh',
    height: '100%',
  },

  content: {
    position: 'relative',
    width: '90%',
    top: '0px',
    left: '50%',
    transform: 'translateX(-50%)',
    minHeight: '100vh',
  },

  recipeInfos: {
    position: 'relative',
    display: 'flex',
    top: '0px',
    marginBottom: '20px',
  },

  recipeInfosAvatar: {
    width: '25%',
    minWidth: '220px',
    position: 'relative',
  },

  avatar: {
    width: '75%',
    height: '75%',
    left: '50%',
    top: '50%',
    transform: 'translateX(-50%) translateY(-50%)',
    borderRadius: '2px',
  },

  chief: {
    position: 'absolute',
    bottom: '0%',
    left: '50%',
    transform: 'translateX(-50%)',
    color: theme.palette.secondary.main,
  },

  recipeInfosText: {
    paddingTop: '10px',
    paddingLeft: '10px',
    minHeight: '100%',
    width: '40%',
    minWidth: '350px',
    display: 'flex',
    flexDirection: 'column',
    right: '0px',
  },

  recipeDescription: {
    width: '35%',
    minWidth: '320px',
  },

  recipeDescriptionText: {
    paddingTop: '10px',
    width: '80%',
    wordWrap: 'break-word',
  },

  recipeName: {
    fontSize: '25px',
  },

  time: {
    marginLeft: '10px',
    marginTop: '10px',
    marginRight: '10px',
    fontSize: '16px',
    display: 'inline',
  },

  timeIcon: {
    color: theme.palette.element.main,
  },

  yield: {
    marginLeft: '10px',
    display: 'inline',
    fontSize: '16px',
  },

  yieldIcon: {
    marginLeft: '10px',
    marginTop: '10px',
    color: theme.palette.element.main,
  },

  rating: {
    width: '100%',
  },

  averageRating: {
    marginRight: '5px',
    fontWeight: 'bold',
    display: 'inline',
  },

  totalRating: {
    marginRight: '10px',
    display: 'inline',
  },

  tags: {
    position: 'absolute',
    bottom: '0px',
  },

  tag: {
    display: 'inline',
    marginRight: '5px',
    fontStyle: 'italic',
  },

  horizontalLine: {
    borderWidth: '1px',
  },

  recipeContent: {
    display: 'flex',
  },

  title: {
    fontSize: '20px',
  },

  ingredients: {
    width: '40%',
  },

  preparation: {
    width: '60%',
  },

  orderedList: {
    listStyleType: 'none',
  },
};

/*
 * component that represent a recipe page
 */
class RecipePage extends Component {
  constructor(props) {
    super(props);

    const recipe = {
      href: '#',
      chief: {},
      image: null,
      name: 'Aucune nom',
      description:
        'Aucune description',
      preparationTime: 1,
      yield: 1,
      rating: 1,
      totalRating: 1,
      ingredientCategories: [
        {
          category: 'Aucune categorie',
          ingredients: [
            {
              quantity: '0',
              name: 'Aucun ingredient',
            },
          ],
        },
      ],
      steps: [
        {
          iteration: 1,
          explanation:
            'Aucune etape',
        },
      ],
      tags: [],
    };

    this.state = {
      user: props.user,
      recipe,
    };

    this.displayTime = this.displayTime.bind(this);
    this.displayDescription = this.displayDescription.bind(this);
  }

  componentWillMount() {
    const recipeId = this.props.match.params.recipeid;
    if (recipeId) {
      const query = `query{
        recipe(id: "${recipeId}"){
          id
          chief {
            id
          }
          image
          name
          preparationTime
          description
          yield
          feedbacks {
            id
            grade
          }
          totalRating
          numberOfFeedbacks
          ingredientCategories{
            category
            ingredients{
              name
              quantity
            }
          }
          steps{
            stepNumber
            description
          }
          tags
        }
      }`;

      fetch(`${apiURI}/graphql`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          query,
        }),
      })
        .then(r => r.json())
        .then(
          (data) => {
            this.setState({ recipe: data.data.recipe });
          },
        );
    }
  }

  /*
   * executed when the component is mounted
   */
  componentDidMount() {
    document.title = 'Cookbook - Recipe page';
  }

  /*
   * display the preparation time
   */
  displayTime() {
    const time = Math.round((this.state.recipe.preparationTime / 60) * 100) / 100;
    const hour = Math.floor(time);
    const minute = Math.round((time - hour) * 60);
    return `${hour}h ${minute}min`;
  }

  /*
   * display the description
   */
  displayDescription() {
    if (this.state.recipe.description && this.state.recipe.description !== '') {
      const { classes } = this.props;
      return (
        <div>
          <Typography className={classes.recipeName}>Description</Typography>
          <Typography className={classes.recipeDescriptionText}>
            {this.state.recipe.description}
          </Typography>
        </div>
      );
    }
    return null;
  }

  render() {
    const { classes } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <Navigator user={this.state.user} />
        <main className={classes.background}>
          <div className={classes.content}>
            <div className={classes.recipeInfos}>
              <div className={classes.recipeInfosAvatar}>
                <UserAvatar avatar={crepe} className={classes.avatar} />
                <Link href={`/${this.state.recipe.chief.id}`} className={classes.chief}>
                  {this.state.recipe.chief.name}
                  {"'s"}
                </Link>
              </div>

              <div className={classes.recipeInfosText}>
                <Typography className={classes.recipeName}>{this.state.recipe.name}</Typography>
                <div>
                  <FontAwesomeIcon icon={faStopwatch} className={classes.timeIcon} />
                  <Typography className={classes.time}>{this.displayTime()}</Typography>
                  <FontAwesomeIcon icon={faUtensils} className={classes.yieldIcon} />
                  <Typography className={classes.yield}>
                    {this.state.recipe.yield}
                    {' serving'}
                    {Util.plural(this.state.recipe.yield)}
                  </Typography>
                </div>
                <div>
                  <p>
                    <Rating rating={this.state.recipe.totalRating} recipeId={this.state.recipe.id} user={this.state.user} />
                  </p>
                </div>
                <div className={classes.rating}>
                  <Typography className={classes.averageRating}>
                    {`${Math.round(this.state.recipe.totalRating * 10) / 10}/5`}
                  </Typography>
                  <Typography className={classes.totalRating}>
                    {'by '}
                    {this.state.recipe.numberOfFeedbacks}
                    {' rating'}
                    {Util.plural(this.state.recipe.totalRating)}
                  </Typography>
                </div>
                <div className={classes.tags}>
                  {this.state.recipe.tags.map(tag => (
                    <Typography className={classes.tag}>
                      {`#${
                        tag
                      }${this.state.recipe.tags.indexOf(tag) === this.state.recipe.tags.length - 1 ? '' : ','}`}
                    </Typography>
                  ))}
                </div>
              </div>
              <div className={classes.recipeDescription}>{this.displayDescription()}</div>
            </div>
            <hr className={classes.horizontalLine} />
            <div className={classes.recipeContent}>
              <div className={classes.ingredients}>
                <Typography className={classes.title}>Ingredients</Typography>
                <ol className={classes.orderedList}>
                  {this.state.recipe.ingredientCategories.map(category => (
                    <li>
                      <Ingredient ingredient={category} />
                    </li>
                  ))}
                </ol>
              </div>
              <div className={classes.preparation}>
                <Typography className={classes.title}>Preparation</Typography>
                <ol className={classes.orderedList}>
                  {this.state.recipe.steps.map(step => (
                    <li>
                      <Step step={step} />
                    </li>
                  ))}
                </ol>
              </div>
            </div>
          </div>
        </main>
      </MuiThemeProvider>
    );
  }
}

RecipePage.propTypes = {
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  match: PropTypes.object,
};

export default withStyles(styles)(RecipePage);
