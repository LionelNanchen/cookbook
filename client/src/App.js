import React from "react";
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from "react-router";
import { BrowserRouter } from "react-router-dom";

import AuthProvider from './AuthProvider';
import { AuthContext } from './AuthProvider';

import SigninPage from './routes/SigninPage';
import SignupPage from './routes/SignupPage';
import UserPage from './routes/UserPage';
import CreateNewRecipePage from './routes/CreateNewRecipePage';
import RecipePage from './routes/RecipePage';
import SearchPage from './routes/SearchPage';
import UserSearchPage from './routes/UserSearchPage';
import PageNotFound from './routes/PageNotFound';

const ProtectedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(params) => (
    <AuthContext>
      {({ user }) => user
        ? <Component {...params} user={user}/>
        : <Redirect to={'/signin'} />}
    </AuthContext>
  )}
  />
)

export default () => (
  <BrowserRouter>
    <AuthProvider>
      <Switch>
        <ProtectedRoute path="/" exact component={UserPage} />
        <ProtectedRoute path="/createrecipe" component={CreateNewRecipePage} />
        <ProtectedRoute path="/users/:userid" component={UserPage} />
        <ProtectedRoute path="/recipes/:recipeid" component={RecipePage} />
        <ProtectedRoute path="/search" component={SearchPage} />
        <ProtectedRoute path="/usersearch" component={UserSearchPage} />
        <Route path="/signin" component={SigninPage} />
        <Route path="/signup" component={SignupPage} />
        <Route component={PageNotFound} />
      </Switch>
    </AuthProvider>
  </BrowserRouter>
);

ProtectedRoute.propTypes = {
  component: PropTypes.object.isRequired,
};
