const Recipe = require('../../models/recipe.model');
const User = require('../../models/user.model');

module.exports = {
  Query: {
    async recipe(parent, { id }) {
      const res = await Recipe.findById(id);
      return res;
    },
    async recipes() {
      const res = await Recipe.find();
      return res;
    },

    async search_recipe_by_name(parent, args) {
      const res = await Recipe.find(
        { name: { $regex: args.research, $options: 'i' } },
      ).sort({ totalRating: 'descending', numberOfFeedbacks: 'descending' });
      return res;
    },

    async search_recipe_by_tag(parent, args) {
      const res = await Recipe.find(
        { tags: { $regex: args.research, $options: 'i' } },
      ).sort({ totalRating: 'descending', numberOfFeedbacks: 'descending' });
      return res;
    },

    async search_recipe(parent, args) {
      let res = null;
      if (args.containedTags.length === 0) {
        res = await Recipe.find(
          {
            name: { $regex: args.name, $options: 'i' },
            tags: {
              $nin: args.notContainedTags,
            },
          },
        ).sort({ totalRating: 'descending', numberOfFeedbacks: 'descending' });
      } else {
        res = await Recipe.find(

          {
            name: { $regex: args.name, $options: 'i' },
            tags: {
              $all: args.containedTags,
              $nin: args.notContainedTags,
            },
          },
        ).sort({ totalRating: 'descending', numberOfFeedbacks: 'descending' });
      }
      return res;
    },
  },

  Recipe: {
    async chief(parent) {
      const res = await User.findById(parent.chief);
      return res;
    },
  },

  Mutation: {
    async create_recipe(parent, args) {
      const recipe = args;
      recipe.creationDate = new Date();
      recipe.lastUpdate = new Date();
      recipe.totalRating = 0.0;
      recipe.numberOfFeedbacks = 0;
      const res = await Recipe.create(recipe);
      const chief = await User.findByIdAndUpdate(
        args.chief,
        { $addToSet: { recipes: res.id } },
        { new: true },
      );
      const results = [];
      // eslint-disable-next-line no-restricted-syntax
      for (const follower of chief.followers) {
        results.push(
          User.findByIdAndUpdate(
            follower,
            {
              $push:
              {
                notifications:
                {
                  date: new Date(),
                  text: 'added a new recipe!',
                  path: `recipes/${res.id}`,
                  read: false,
                  authorId: res.chief,
                  authorName: chief.name,
                  authorPicture: chief.picture,
                },
              },
            },
            { new: true },
          ).exec(),
        );
      }
      await Promise.all(results);
      return res;
    },

    async add_feedback(parent, args) {
      const recipe = await Recipe.findOne(
        {
          _id: args.recipe,
          'feedbacks.author': args.feedback.author,
        },
      );

      let res = null;
      if (recipe == null) {
        res = await Recipe.findByIdAndUpdate(
          args.recipe,
          {
            $addToSet: {
              feedbacks: {
                author: args.feedback.author,
                grade: args.feedback.grade,
                comment: args.feedback.comment,
              },
            },
          },
          { new: true },
        );
      } else {
        res = await Recipe.findOneAndUpdate(
          { _id: args.recipe, 'feedbacks.author': args.feedback.author },
          {
            $set: {
              'feedbacks.$': {
                author: args.feedback.author,
                grade: args.feedback.grade,
                comment: args.feedback.comment,
              },
            },
          },
          { new: true },
        );
      }

      const author = await User.findById(args.feedback.author);

      // we use a array of results to wait for the two next promises to be execute in parallel
      const results = [];

      // Sending a notification to the chief
      results.push(
        User.findByIdAndUpdate(
          res.chief,
          {
            $addToSet: {
              notifications: {
                date: new Date(),
                text: `rated your recipe: ${res.name}`,
                path: `recipes/${res.id}`,
                read: false,
                authorId: author.id,
                authorName: author.name,
                authorPicture: author.picture,
              },
            },
          },
        ).exec(),
      );

      // calcul of recipe total ranking
      let ranking = 0;
      for (let i = 0; i < res.feedbacks.length; i += 1) {
        ranking += res.feedbacks[i].grade;
      }
      ranking /= res.feedbacks.length;

      // send the updated total rating in database
      results.push(
        Recipe.findByIdAndUpdate(
          args.recipe,
          {
            $set: {
              totalRating: ranking,
              numberOfFeedbacks: res.feedbacks.length,
            },
          },
        ).exec(),
      );
      await Promise.all(results);
      // we update the value of the recipe total rating before returning the result
      res.totalRating = ranking;
      return res;
    },


  },

};
