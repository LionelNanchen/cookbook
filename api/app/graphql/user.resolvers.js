const User = require('../../models/user.model');
const Recipe = require('../../models/recipe.model');

module.exports = {
  Query: {
    async user(parent, { id }) {
      const res = await User.findById(id);
      return res;
    },
    async users() {
      const res = await User.find();
      return res;
    },
    async login_user(parent, args) {
      const res = await User.findOne(
        { email: args.email, password: args.password },
      );
      const resUpdated = await User.findByIdAndUpdate(
        res.id,
        {
          $set: { lastConnectionDate: new Date() },
        },
        { new: true },
      );
      return resUpdated;
    },

    async search_user(parent, args) {
      const res = await User.find(
        { name: { $regex: args.research, $options: 'i' } },
      ).sort({ name: 'ascending' });
      return res;
    },

  },

  User: {
    async recipes(parent) {
      const res = await Recipe.find({ _id: { $in: parent.recipes } });
      return res;
    },

    async followers(parent) {
      const res = await User.find({ _id: { $in: parent.followers } });
      return res;
    },
    async followed(parent) {
      const res = await User.find({ _id: { $in: parent.followed } });
      return res;
    },
  },

  Mutation: {
    async create_user(parent, args) {
      const res = await User.create({
        name: args.name,
        email: args.email,
        password: args.password,
        inscriptionDate: new Date(),
      });
      return res;
    },

    async follow_user(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        { $addToSet: { followed: args.followed_user } },
        { new: true },
      );
      await User.findByIdAndUpdate(
        args.followed_user,
        {
          $addToSet: { followers: args.user },
          $push: {
            notifications: {
              date: new Date(),
              text: 'started following you!',
              path: `users/${args.user}`,
              read: false,
              authorId: args.user,
              authorName: res.name,
              authorPicture: res.picture,
            },
          },
        },
        { new: true },
      );
      return res;
    },

    async unfollow_user(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        { $pull: { followed: args.followed_user } },
        { new: true },
      );
      await User.findByIdAndUpdate(
        args.followed_user,
        {
          $pull: { followers: args.user },
        },
        { new: true },
      );
      return res;
    },

    async add_notification(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        {
          $addToSet: {
            notifications: {
              date: new Date(),
              text: args.notification.text,
              path: args.notification.path,
              read: false,
              authorId: args.notification.authorId,
              authorName: args.notification.authorName,
              authorPicture: args.notification.authorPicture,
            },
          },
        },
        { new: true },
      );
      return res;
    },

    async delete_notification(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        {
          $pull:
          {
            notifications: {
              _id: args.notification,
            },
          },
        },
        { new: true },
      );
      return res;
    },

    async update_user_email(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        { $set: { email: args.email } },
        { new: true },
      );
      return res;
    },

    async update_user_password(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        { $set: { password: args.password } },
        { new: true },
      );
      return res;
    },

    async update_user_picture(parent, args) {
      const res = await User.findByIdAndUpdate(
        args.user,
        { $set: { picture: args.picture } },
        { new: true },
      );
      return res;
    },
  },
};
