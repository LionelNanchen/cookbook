const Notification = require('../../models/notification.model');

module.exports = {
  Query: {
    async notification(parent, { id }) {
      const res = await Notification.findById(id);
      return res;
    },
    async notifications() {
      const res = await Notification.find();
      return res;
    },
  },
  Mutation: {
    async create_notification(parent, args) {
      const res = await Notification.create(args);
      return res;
    },
  },
};
