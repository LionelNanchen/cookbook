const merge = require('lodash/merge');
const fs = require('fs');
const path = require('path');
const { makeExecutableSchema } = require('graphql-tools');
// individual schemas for merge
const exampleResolvers = require('./example.schema');
const userResolvers = require('./user.resolvers');
const recipeResolvers = require('./recipe.resolvers');
const date = require('./date');

module.exports = makeExecutableSchema({
  typeDefs: fs.readFileSync(path.join(__dirname, '../api.graphql'), 'utf-8'),
  resolvers: merge(
    date,
    exampleResolvers,
    userResolvers,
    recipeResolvers,
  ),
});
