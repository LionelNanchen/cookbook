module.exports = {
  url: '',
  port: process.env.PORT || 8000,
  jwtOptions: {
    secret: process.env.JWT_SECRET || '12345ABCDE',
  },
  mongoDB: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/cookbook',
};
