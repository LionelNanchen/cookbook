require('dotenv/config');
const app = require('./app');
const { port } = require('./config');

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is up and running on port: http://localhost:${port}`);
});
