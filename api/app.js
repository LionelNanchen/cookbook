// app.js
require('dotenv/config');

const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const bodyParser = require('body-parser');
const cors = require('cors');
const { graphqlExpress, graphiqlExpress } = require('graphql-server-express');
const schema = require('./app/graphql/executable.schema');

// import all routs
const example = require('./routes/example.route');
const exampleprivate = require('./routes/exampleprivate.route');
const auth = require('./routes/auth.route');

// import needed values from config
const { mongoDB } = require('./config');

const app = express();

// add middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(passport.initialize());

// set up mongoose connection
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// add all needed routes
app.use('/example', example);
app.use('/exampleprivate', exampleprivate);
app.use('/auth', auth);

// graphql
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

app.use('/graphiql', graphiqlExpress({
  endpointURL: '/graphql',
}));

// error management
app.use((err, res) => {
  // console.error(err); // log the error in the console
  res.status(500).send('Woops something went wrong, please contact the administrator');
});

module.exports = app;
