const mongoose = require('mongoose');

const { Schema } = mongoose.Schema;

const NotificationSchema = new Schema({
  timestamp: { type: Number, required: true },
  text: { type: String, required: true },
  path: { type: String, required: true },
  authorId: { type: String },
  authorName: { type: String },
  authorPicture: { type: String },
});

// Export the model
module.exports = mongoose.model('Notification', NotificationSchema);
