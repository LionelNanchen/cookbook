const mongoose = require('mongoose');

const { Schema, ObjectId } = mongoose;

ObjectId.prototype.valueOf = () => this.toString();

const Feedback = new Schema({
  author: { type: ObjectId, ref: 'User', required: true },
  grade: { type: Number },
  comment: { type: String },
});

const Ingredient = new Schema({
  name: { type: String, required: true, max: 100 },
  quantity: { type: String, required: true },
});

const IngredientCategory = new Schema({
  category: { type: String, required: true, max: 100 },
  ingredients: [Ingredient],
});

const Step = new Schema({
  stepNumber: { type: Number, required: true },
  title: { type: String, max: 100 },
  description: { type: String, required: true },
  advice: { type: String },
});

const RecipeSchema = new Schema({
  chief: { type: ObjectId, ref: 'User', required: true },
  image: { type: String },
  name: { type: String, required: true, max: 100 },
  preparationTime: { type: Number },
  description: { type: String },
  yield: { type: Number },
  feedbacks: [Feedback],
  totalRating: { type: Number },
  numberOfFeedbacks: { type: Number },
  creationDate: { type: Date },
  lastUpdate: { type: Date },
  ingredientCategories: [IngredientCategory],
  steps: [Step],
  tags: [{ type: String }],
});

// Export the model
module.exports = mongoose.model('Recipe', RecipeSchema);
