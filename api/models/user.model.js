const mongoose = require('mongoose');

const { Schema, ObjectId } = mongoose;

const Notification = new Schema({
  date: { type: Date, required: true },
  text: { type: String, required: true },
  path: { type: String },
  read: { type: Boolean, required: true },
  authorId: { type: String },
  authorName: { type: String },
  authorPicture: { type: String },
});

const UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, max: 100 },
  password: { type: String, required: true, max: 100 },
  picture: { type: String },
  inscriptionDate: { type: Date },
  lastConnectionDate: { type: Date },
  recipes: [{ type: ObjectId, ref: 'Recipe' }],
  followers: [{ type: ObjectId, ref: 'User' }],
  followed: [{ type: ObjectId, ref: 'User' }],
  notifications: [Notification],
});

// Export the model
module.exports = mongoose.model('User', UserSchema);
