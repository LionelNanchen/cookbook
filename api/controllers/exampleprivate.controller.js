const ExamplePrivate = require('../models/exampleprivate.model');

// simple version, without validation or sanitation
exports.example = (req, res) => {
  res.send('Greetings from the Example !!!! PRIVATE !!!! controller!');
};

exports.example_create = (req, res) => {
  const example = new ExamplePrivate(
    {
      name: req.body.name,
      price: req.body.price,
    },
  );

  example.save((err) => {
    if (err) { return console.error(err); }
    res.send('Example Created successfully');
    return true;
  });
};

exports.example_details = (req, res) => {
  ExamplePrivate.find({ name: req.params.name }, (err, example) => {
    if (err) return console.error(err);
    res.send(example);
    return true;
  });
};

exports.example_update = (req, res) => {
  ExamplePrivate.findByIdAndUpdate(req.params.id, { $set: req.body }, (err) => {
    if (err) return console.error(err);
    res.send('Example udpated.');
    return true;
  });
};

exports.example_delete = (req, res) => {
  ExamplePrivate.findByIdAndRemove(req.params.id, (err) => {
    if (err) return console.error(err);
    res.send('Example deleted.');
    return true;
  });
};
