const jwt = require('jsonwebtoken');
const passport = require('passport');
const passportLocal = require('passport-local');
const passportJWT = require('passport-jwt');
const request = require('request');

const { jwtOptions, port } = require('../config');

const LocalStrategy = passportLocal.Strategy;
const JWTStrategy = passportJWT.Strategy;
const { ExtractJwt } = passportJWT;

passport.use(new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
  },
  (username, password, done) => {
    const query = `query{
      login_user(email: "${username}", password: "${password}"){
        id
        name
        email
        picture
        recipes {
          id
          chief {
            id
            name
          }
          image
          name
          preparationTime
          description
          yield
          ingredientCategories {
            category
            ingredients {
              name
              quantity
            }
          }
          steps {
            stepNumber
            description
          }
          tags
        }
        followers {
          name
        }
        followed {
          id
          name
          recipes{
            id
            chief {
              id
              name
            }
            image
            name
            preparationTime
            description
            yield
            ingredientCategories {
              category
              ingredients {
                name
                quantity
              }
            }
            steps {
              stepNumber
              description
            }
            tags
          }
        }
        notifications {
          id
          date
          read
          authorId
          authorName
          authorPicture
          text
          path
        }
      }
    }`;
    request.post({
      uri: `http://localhost:${port}/graphql`,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query }),
    }, (err, res, user) => {
      const myData = JSON.parse(user);
      const myUser = myData.data.login_user;

      if (err) return done(null, false);
      return done(null, myUser);
    });
  },
));

passport.use(new JWTStrategy(
  {
    secretOrKey: jwtOptions.secret,
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  },
  (jwtPayload, done) => {
    const { userId } = jwtPayload;

    const query = `query{
      user(id: "${userId}"){
        id
        name
        email
        picture
        recipes {
          id
          chief {
            id
            name
          }
          image
          name
          preparationTime
          description
          yield
          ingredientCategories {
            category
            ingredients {
              name
              quantity
            }
          }
          steps {
            stepNumber
            description
          }
          tags
        }
        followers {
          name
        }
        followed {
          id
          name
          recipes{
            id
            chief {
              id
              name
            }
            image
            name
            preparationTime
            description
            yield
            ingredientCategories {
              category
              ingredients {
                name
                quantity
              }
            }
            steps {
              stepNumber
              description
            }
            tags
          }
        }
        notifications {
          id
          date
          read
          authorId
          authorName
          authorPicture
          text
          path
        }
      }
    }`;

    request.post({
      uri: `http://localhost:${port}/graphql`,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ query }),
    }, (err, res, user) => {
      const myData = JSON.parse(user);
      const myUser = myData.data.user;
      if (err) return done(null, false);
      return done(null, myUser);
    });
  },
));

exports.passportMiddleware = passport.authenticate('local', { session: false });

exports.login = (req, res) => {
  const { ...user } = req.user; // copy the user without the password
  const token = jwt.sign({ userId: user.id }, jwtOptions.secret);
  res.send({ user, token });
};
