const Example = require('../models/example.model');

// simple version, without validation or sanitation
exports.example = (req, res) => {
  res.send({ text: 'Greetings from the Example controller!' });
};

exports.example_create = (req, res) => {
  const example = new Example(
    {
      name: req.body.name,
      price: req.body.price,
    },
  );

  example.save((err) => {
    if (err) { return console.error(err); }
    // res.send({'text':'Example Created successfully'});
    res.send(example);
    return true;
  });
};

exports.example_details = (req, res) => {
  Example.findOne({ name: req.params.name }, (err, example) => {
    if (err) return console.error(err);
    res.send(example);
    return true;
  });
};

exports.example_update = (req, res) => {
  // args are
  // the filter parameter, here it is id
  // the update values (contained in the body)
  // returnNewDocuments returns the updated document not the old one
  // last but not lease, the function (err, example) gives us access
  // to the newly updated object: example
  Example.findOneAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
    (err, updatedExample) => {
      if (err) return console.error(err);
      res.send(updatedExample);
      return true;
    },
  );
};

exports.example_delete = (req, res) => {
  Example.findByIdAndRemove(req.params.id, (err) => {
    if (err) return console.error(err);
    res.send({ text: 'Example deleted.' });
    return true;
  });
};
