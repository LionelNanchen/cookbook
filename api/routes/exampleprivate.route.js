const express = require('express');
const passport = require('passport');

const router = express.Router();

// Require the controllers
const exampleprivateController = require('../controllers/exampleprivate.controller');

const authenticated = () => passport.authenticate('jwt', { session: false });

// a simple test url to check that all of our files are communicating correctly.
router.get('/example', authenticated(), exampleprivateController.example);

// C
router.post('/create', authenticated(), exampleprivateController.example_create);

// R
router.get('/:name', authenticated(), exampleprivateController.example_details);

// U
router.put('/:id/update', authenticated(), exampleprivateController.example_update);

// D
router.delete('/:id/delete', authenticated(), exampleprivateController.example_delete);

module.exports = router;
