const express = require('express');

const router = express.Router();

// Require the controllers
const exampleController = require('../controllers/example.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/example', exampleController.example);

// C
router.post('/create', exampleController.example_create);

// R
router.get('/:name', exampleController.example_details);

// U
router.put('/:id/update', exampleController.example_update);

// D
router.delete('/:id/delete', exampleController.example_delete);

module.exports = router;
