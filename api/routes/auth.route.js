const express = require('express');
const passport = require('passport');

const router = express.Router();

// Require the controllers
const authController = require('../controllers/auth.controller');

const authenticated = () => passport.authenticate('jwt', { session: false });

router.post('/login', authController.passportMiddleware, authController.login);

router.get(router.get('/me', authenticated(), (req, res) => {
  res.send({ user: req.user });
}));

module.exports = router;
