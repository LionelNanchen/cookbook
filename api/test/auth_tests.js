/**
 * Mocha tests - example
*/

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

// example tests
describe('Auth tests', () => {
  it('Succesfull login with the test credentials', (done) => {
    const user = {
      username: 'test@test.com',
      password: 'test',
    };

    chai.request(app)
      .post('/auth/login')
      .send(user)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('token');
        done();
      });
  });
});

