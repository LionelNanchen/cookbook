/**
 * Mocha tests - example
 */

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');

chai.use(chaiHttp);

// example tests
describe('Example CDRD tests', () => {
  let userId;

  it('returns the greeting line when accessing the /example route', (done) => {
    chai.request(app)
      .get('/example/example')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('text').eql('Greetings from the Example controller!');
        done();
      });
  });

  it('saves an example.modele to the database', (done) => {
    const example = {
      name: 'toto',
      price: 123,
    };
    chai.request(app)
      .post('/example/create')
      .send(example)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql('toto');
          res.body.should.have.property('price').eql(123);
          res.body.should.have.property('_id');
          done();
        }
      });
  });

  it('reads an example model from the database when its name is given in url', (done) => {
    chai.request(app)
      .get('/example/toto')
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql('toto');
          res.body.should.have.property('price').eql(123);
          res.body.should.have.property('_id');
          // eslint-disable-next-line no-underscore-dangle
          userId = res.body._id;
          done();
        }
      });
  });

  it('updates an example model from the database when its id is given in url and the changes in a post', (done) => {
    const example = {
      name: 'tata',
      price: 456,
    };
    chai.request(app)
      .put(`/example/${userId}/update`)
      .send(example)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('name').eql('tata');
          res.body.should.have.property('price').eql(456);
          res.body.should.have.property('_id').eql(userId);
          done();
        }
      });
  });
});
